#!/bin/bash

wget http://localhost:9090/jnlpJars/jenkins-cli.jar
docker run \
-v `pwd`/jenkins-cli.jar:/opt/jenkins-cli.jar \
-v `pwd`/plugins.txt:/opt/plugins.txt \
-v `pwd`/install_plugins.sh:/opt/install_plugins.sh \
--net=host \
openjdk:8 bash /opt/install_plugins.sh
